import React from 'react'
import { useAuth0 } from "@auth0/auth0-react"
import './style.scss'

import Button from '../../atoms/button'
import Link from '../../atoms/link'
import Loader from '../../atoms/loader'

export default function SessionButtons({ isAuthenticated, isLoading }) {
  const { user, loginWithRedirect, logout } = useAuth0();
  const name = user?.name || 'Welcome'

  if (isLoading) {
    return <Loader size='small' />
  }

  if (isAuthenticated) {
    return (
      <React.Fragment>
        <Link as={name} disabled='true' />
        <span className='divider'></span>
        <Button text='Sign Out' onClick={() => logout({ returnTo: window.location.origin })} />
      </React.Fragment>
    )
  }

  return (
    <Button text='Sign In' onClick={() => loginWithRedirect()} />
  )
}