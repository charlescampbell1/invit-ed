import React from 'react'
import SessionButtons from '.'

export default {
  title: 'Molecules/SessionButtons',
  component: SessionButtons
}

const Template = args => <SessionButtons {...args} />

export const SignedOut = Template.bind({})
export const SignedIn = Template.bind({})

SignedOut.args = {
  isAuthenticated: false,
}

SignedIn.args = {
  isAuthenticated: true,
  isLoading: false
}