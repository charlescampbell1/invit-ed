import React from 'react'
import FooterLinks from '.'

export default {
  title: 'Molecules/DefaultFooterLinks',
  component: FooterLinks
}

const Template = args => <FooterLinks {...args} />

export const DefaultFooterLinks = Template.bind({})