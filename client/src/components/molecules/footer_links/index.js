import React from 'react'
import Link from '../../atoms/link'

export default function FooterLinks() {
  return (
    <div className='footer-links'>
      <Link to='/welcome' as='Home' />
      <Link to='/invitations' as='Invitations' />
      <Link to='/invitations/create' as='New Invitation' />
    </div>
  )
}