import React from 'react'
import Table from '.'

export default {
  title: 'Molecules/DefaultTable',
  component: Table
}

const Template = args => <Table {...args} />

export const DefaultTable = Template.bind({})

const columns = [
  {
    name: 'Bride & Groom',
    options: {
      filter: false
    }
  },
  {
    name: 'Location',
    options: {
      filter: true
    }
  }
]

const data = [
  ['Jack & Jill', 'Cornwall'],
  ['John & Jane', 'London']
]

DefaultTable.args = {
  columns: columns,
  data: data
}