import React from 'react'
import MUIDataTable from 'mui-datatables'



export default function Table({ data, columns }) {
  const options = {
    filter: true,
    filterType: 'dropdown',
    responsive: 'standard'
  }

  return (
    <React.Fragment>
      <MUIDataTable data={data} columns={columns} options={options} />
    </React.Fragment>
  )
}