import React from 'react'
import InvitationForm from '.'

export default {
  title: 'Organisms/InvitatonForm',
  component: InvitationForm
}

const Template = args => <InvitationForm {...args} />

export const MuiForm = Template.bind({})

