import React from 'react';
import { Form } from 'react-final-form';
import { Paper, Grid, Button } from '@material-ui/core';
import DateFnsUtils from '@date-io/date-fns';
import { TextField, KeyboardDatePicker } from 'mui-rff';
import api from '../../../api'
import './style.scss'

const formFields = [
  {
    size: 12,
    field: (<TextField label='Bride &amp; Groom' name='names' margin='none' required={true} />)
  },
  {
    size: 7,
    field: (<TextField label='Location' name='location' margin='none' required={true} />)
  },
  {
    size: 5,
    field: (
      <KeyboardDatePicker name='wedding_date' format='dd/MM/yyyy' margin='none' label='Wedding Day' dateFunsUtils={DateFnsUtils} />
    ),
  },
  {
    size: 12,
    field: <TextField name='notes' multiline rows='2' rowsMax='4' label='Invitation Note' margin='none' />,
  }
]

const validate = values => {
  const errors = {};
  if (!values.names) { errors.names = 'Required' }
  if (!values.location) { errors.location = 'Required' }
  if (!values.wedding_date) { errors.wedding_date = 'Required' }
  return errors
}

const onSubmit = async values => {
  await api.insertInvitation(values).then(res => {
    window.alert('Invitation inserted successfully')
  })
}

export default function InvitationForm({ }) {
  return (
    <Form onSubmit={onSubmit} validate={validate} render={({ handleSubmit, form, submitting, pristine, values }) => (
      <form onSubmit={handleSubmit} noValidate>
        <Paper style={{ padding: 16 }}>
          <Grid container alignItems='flex-start' spacing={2}>
            {formFields.map((item, idx) => (
              <Grid item xs={item.size} key={idx}>
                {item.field}
              </Grid>
            ))}

            <Grid item style={{ marginTop: 16 }}>
              <Button type='button' variant='contained' onClick={form.reset} disabled={submitting || pristine}>Reset</Button>
            </Grid>

            <Grid item style={{ marginTop: 16 }}>
              <Button className='submit-button' variant='contained' type="submit" disabled={submitting}>Submit</Button>
            </Grid>
          </Grid>
        </Paper>
      </form>
    )}
    />
  )
}