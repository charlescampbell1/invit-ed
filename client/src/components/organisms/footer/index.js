import React from 'react'
import FooterLinks from '../../molecules/footer_links'
import SessionButtons from '../../molecules/session_buttons'
import { useAuth0 } from "@auth0/auth0-react"
import './style.scss'

export default function Footer() {
  const { isAuthenticated, isLoading } = useAuth0();
  return (
    <div className='footer'>
      <FooterLinks />

      <div className='right'>
        <SessionButtons isAuthenticated={isAuthenticated} isLoading={isLoading} />
      </div>
    </div>
  )
}