import React from 'react'
import Footer from '.'

export default {
  title: 'Organisms/DefaultFooter',
  component: Footer
}

const Template = args => <Footer {...args} />

export const DefaultFooter = Template.bind({})