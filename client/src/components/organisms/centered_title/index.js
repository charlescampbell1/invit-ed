import React from 'react'
import './style.scss'

import Title from '../../atoms/title'

export default function CenteredTitle({ text }) {
  return (
    <div className='title-container'>
      <Title text={text} />
    </div>
  )
}