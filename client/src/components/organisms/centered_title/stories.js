import React from 'react'
import CenteredTitle from '.'

export default {
  title: 'Organisms/CenteredTitle',
  component: CenteredTitle
}

const Template = args => <CenteredTitle {...args} />

export const CentrePageTitle = Template.bind({})

CentrePageTitle.args = {
  text: 'INVIT•ED'
}