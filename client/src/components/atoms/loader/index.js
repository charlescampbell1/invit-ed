import React from 'react'
import { Bars } from '@agney/react-loading'
import './style.scss'

export default function Loader({ size }) {
  return (
    <div className={`loader ${size}`}>
      <Bars />
    </div>
  )
}