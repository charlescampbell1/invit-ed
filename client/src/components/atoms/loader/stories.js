import React from 'react'
import Loader from '.'

export default {
  title: 'Atoms/BarLoader',
  component: Loader
}

const Template = args => <Loader {...args} />

export const LargeLoader = Template.bind({})

LargeLoader.args = {
  size: 'large'
}