import React from 'react'
import './style.scss'

export default function Link({ status, active, to, as, disabled }) {
  if (active) { active = 'active' }
  if (disabled) { disabled = 'disabled' }
  return <a className={`link ${status} ${active} ${disabled}`} href={to}>{as}</a>
}