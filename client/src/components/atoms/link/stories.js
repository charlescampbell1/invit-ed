import React from 'react'
import { Component } from 'react'
import Link from '.'

export default {
  title: 'Atoms/Links',
  component: Link
}

const Template = args => <Link {...args} />

export const Default = Template.bind({})
export const Disabled = Template.bind({})

Default.args = {
  to: '/link',
  as: 'Home',
  active: false,
  status: ''
}

Disabled.args = {
  as: 'Disabled',
  disabled: true
}