import React from 'react'
import Title from '.'

export default {
  title: 'Atoms/Title',
  component: Title
}

const Template = args => <Title {...args} />

export const PageTitle = Template.bind({})

PageTitle.args = {
  text: 'INVIT•ED'
}