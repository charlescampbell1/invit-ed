import React from 'react'
import './style.scss'

export default function Button({ status, text, onClick }) {
  return <button className={`footer-button ${status}`} onClick={onClick}> {text}</button >
}
