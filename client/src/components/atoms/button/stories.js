import React from 'react'
import Button from '.'

export default {
  title: 'Atoms/Buttons',
  component: Button
}

const Template = args => <Button {...args} />

export const Default = Template.bind({})
export const Danger = Template.bind({})
export const Success = Template.bind({})
export const Login = Template.bind({})

Default.args = {
  status: '',
  text: 'Default',
  onClick: ''
}

Danger.args = {
  status: 'danger',
  text: 'Danger',
  onClick: ''
}

Success.args = {
  status: 'success',
  text: 'Success',
  onClick: '',
}

Login.args = {
  text: 'Sign In',
  onClick: ''
}


