import React from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { Invitation, Invitations, InvitationsCreate, InvitationsUpdate, Landing } from '../pages'

import 'bootstrap/dist/css/bootstrap.min.css'
import { DefaultFooter } from '../components/organisms/footer/stories'

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" render={() => (<Redirect to="/welcome" />)} />
        <Route path='/welcome' exact component={Landing} />
        <Route path='/invitation/:id' exact component={Invitation} />
        <Route path='/invitations' exact component={Invitations} />
        <Route path="/invitations/create" exact component={InvitationsCreate} />
        <Route path="/invitations/update/:id" exact component={InvitationsUpdate} />
      </Switch>
      <DefaultFooter />
    </Router>


  )
}

export default App