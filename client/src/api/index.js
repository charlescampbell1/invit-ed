import axios from 'axios'

const api = axios.create({ baseURL: `http://localhost:3000/api` })

export const insertInvitation = payload => api.post(`/invitation`, payload)
export const getAllInvitations = () => api.get(`/invitations`)
export const updateInvitationById = (id, payload) => api.put(`/invitation/${id}`, payload)
export const deleteInvitationById = id => api.delete(`/invitation/${id}`)
export const getInvitationById = id => api.get(`/invitation/${id}`)

const apis = {
  insertInvitation, getAllInvitations, updateInvitationById, deleteInvitationById, getInvitationById
}

export default apis