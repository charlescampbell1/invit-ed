import React, { Component } from 'react'

import CenteredTitle from '../../components/organisms/centered_title'

export default function Landing() {
  return <CenteredTitle text='invit•ed' />
}