import React, { Component } from 'react'
import Moment from 'moment'
import api from '../../api'
import styled from 'styled-components'
import { withAuthenticationRequired } from '@auth0/auth0-react'
import { DefaultTable } from '../../components/molecules/table/stories'

import Title from '../../components/atoms/title'

import './style.scss'

const Show = styled.div`color: #01afff; cursor: pointer;`
const Update = styled.div`color: #ef9b0f; cursor: pointer;`
const Delete = styled.div`color: #ff0000; cursor: pointer;`

class ShowInvitation extends Component {
  showUser = event => {
    event.preventDefault()
    window.location.href = `/invitation/${this.props.id}`
  }

  render() { return <Show onClick={this.showUser}>Show</Show> }
}

class UpdateInvitation extends Component {
  updateUser = event => {
    event.preventDefault()
    window.location.href = `/invitations/update/${this.props.id}`
  }

  render() {
    return <Update onClick={this.updateUser}>Update</Update>
  }
}

class DeleteInvitation extends Component {
  deleteUser = event => {
    event.preventDefault()
    if (window.confirm(`Do you want to delete the invitation ${this.props.id} permanently?`)) {
      api.deleteInvitationById(this.props.id)
      window.location.reload()
    }
  }

  render() {
    return <Delete onClick={this.deleteUser}>Delete</Delete>
  }
}

class Invitations extends Component {
  constructor(props) {
    super(props)
    this.state = {
      invitations: [],
      columns: [],
      isLoading: false,
    }
  }

  componentDidMount = async () => {
    this.setState({ isLoading: true })

    await api.getAllInvitations().then(invitations => {
      this.setState({
        invitations: invitations.data.data,
        isLoading: false,
      })
    })
  }

  render() {
    const { isLoading } = this.state

    const dataRows = []

    this.state.invitations.forEach((item, i) => {
      dataRows.push(
        [
          item.names,
          item.location,
          Moment(item.wedding_date).format('D / MM / YYYY'),
          item.notes,
          <ShowInvitation id={item._id} />,
          <UpdateInvitation id={item._id} />,
          <DeleteInvitation id={item._id} />
        ]
      )
    })

    const columns = [
      { name: 'Bride & Groom', options: { filter: false } },
      { name: 'Location', options: { filter: false } },
      { name: 'Wedding Date', options: { filter: true } },
      { name: 'Notes', options: { filter: false, sort: false } },
      { name: 'Manage', options: { filter: false, sort: false } },
      { name: '', options: { filter: false, sort: false } },
      { name: '', options: { filter: false, sort: false } },
    ]

    return (
      <div className='page-container'>
        <Title text='Invitations' />
        <DefaultTable columns={columns} data={dataRows} />
      </div>
    )
  }
}

export default withAuthenticationRequired(Invitations)