import React, { Component } from 'react'
import Moment from 'moment'
import api from '../../api'
import './invitation.css'

class Invitation extends Component {
  constructor(props) {
    super(props)

    this.state = {
      id: this.props.match.params.id,
      names: '',
      location: '',
      wedding_date: '',
      notes: ''
    }
  }

  componentDidMount = async () => {
    const { id } = this.state
    const invitation = await api.getInvitationById(id)

    this.setState({
      names: invitation.data.data.names,
      location: invitation.data.data.location,
      wedding_date: invitation.data.data.wedding_date,
      notes: invitation.data.data.notes
    })
  }

  render() {
    const { names, location, wedding_date, notes } = this.state

    return (
      <React.Fragment>
        <div className='bg'>
          <div className='content'>
            <span className='names'>{names}</span>
            <hr />
            <span className='location'>{location}</span>
            <span className='date'>{Moment(wedding_date).format('dddd Do MMMM YYYY')}</span>
            <span className='note'>{notes}</span>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default Invitation