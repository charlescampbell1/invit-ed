import React, { Component } from 'react'
import api from '../../api'
import styled from 'styled-components'

const Title = styled.h1.attrs({ className: 'h1' })``
const Wrapper = styled.div.attrs({ className: 'form-group' })`margin: 0 30px;`
const Label = styled.label`margin: 5px;`
const InputText = styled.input.attrs({ className: 'form-control', })`margin: 5px;`
const Button = styled.button.attrs({ className: `btn btn-primary`, })`margin: 15px 15px 15px 5px;`
const CancelButton = styled.a.attrs({ className: `btn btn-danger`, })`margin: 15px 15px 15px 5px;`

class InvitationsUpdate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      id: this.props.match.params.id,
      names: '',
      location: '',
      wedding_date: '',
      notes: ''
    }
  }

  handleChangeInputNames = async event => {
    const names = event.target.value
    this.setState({ names })
  }

  handleChangeInputLocation = async event => {
    const location = event.target.value
    this.setState({ location })
  }

  handleChangeInputWeddingDate = async event => {
    const wedding_date = event.target.value
    this.setState({ wedding_date })
  }

  handleChangeInputNotes = async event => {
    const notes = event.target.value
    this.setState({ notes })
  }

  handleUpdateInvitation = async () => {
    const { id, names, location, wedding_date, notes } = this.state
    const payload = { names, location, wedding_date, notes }

    await api.updateInvitationById(id, payload).then(res => {
      window.alert(`Invitation updated successfully`)
      this.setState({
        names: '',
        location: '',
        wedding_date: '',
        notes: ''
      })
      this.props.history.push('/invitations')
    })
  }

  componentDidMount = async () => {
    const { id } = this.state
    const invitation = await api.getInvitationById(id)

    this.setState({
      names: invitation.data.data.names,
      location: invitation.data.data.location,
      wedding_date: invitation.data.data.wedding_date,
      notes: invitation.data.data.notes
    })
  }

  render() {
    const { names, location, wedding_date, notes } = this.state
    return (
      <Wrapper>
        <Title>Update Invitation</Title>

        <Label>Names</Label>
        <InputText type="text" value={names} onChange={this.handleChangeInputNames} />

        <Label>Location</Label>
        <InputText type="text" value={location} onChange={this.handleChangeInputLocation} />

        <Label>Wedding Date</Label>
        <InputText type="date" value={wedding_date} onChange={this.handleChangeInputWeddingDate} />

        <Label>Notes</Label>
        <InputText type="text" value={notes} onChange={this.handleChangeInputNotes} />


        <Button onClick={this.handleUpdateInvitation}>Update Invitation</Button>
        <CancelButton href={'/invitations'}>Cancel</CancelButton>
      </Wrapper>
    )
  }
}

export default InvitationsUpdate