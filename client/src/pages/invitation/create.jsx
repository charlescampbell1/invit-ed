import React, { Component } from 'react'
import Title from '../../components/atoms/title';
import InvitationForm from '../../components/organisms/invitation_form';
import './style.scss'

class InvitationsCreate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      names: '',
      location: '',
      wedding_date: '',
      notes: ''
    }
  }

  render() {
    return (
      <div className='form-card'>
        <Title text='New Invitation' />
        <InvitationForm />
      </div>
    )
  }
}

export default InvitationsCreate