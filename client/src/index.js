import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import { Auth0Provider } from '@auth0/auth0-react'

const domain = "invited.eu.auth0.com"
const clientId = "V2tNtCXmsrt3dXSZ5lAtmgQ3i8B8c21R"
const redirectUri = 'http://localhost:8000/welcome'

ReactDOM.render(
  <Auth0Provider domain={domain} clientId={clientId} redirectUri={redirectUri}>
    <App />
  </Auth0Provider>,
  document.getElementById("root")
);