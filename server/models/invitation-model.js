const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Invitation = new Schema(
  {
    names: { type: String, required: true },
    location: { type: String, required: true },
    wedding_date: { type: Date, required: true },
    notes: { type: String }
  },
  { timestamps: true }
)

module.exports = mongoose.model('invitations', Invitation)