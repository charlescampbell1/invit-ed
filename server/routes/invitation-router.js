const express = require('express')
const InvitationCtrl = require('../controllers/invitation-ctrl')
const router = express.Router()

router.post('/invitation', InvitationCtrl.createInvitation)
router.put('/invitation/:id', InvitationCtrl.updateInvitation)
router.delete('/invitation/:id', InvitationCtrl.deleteInvitation)
router.get('/invitation/:id', InvitationCtrl.getInvitationById)
router.get('/invitations', InvitationCtrl.getInvitations)

module.exports = router

