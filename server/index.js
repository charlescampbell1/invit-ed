require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const db = require('./db')
const invitationRouter = require('./routes/invitation-router')
const { auth } = require('express-openid-connect')

const config = {
  authRequired: false,
  auth0Logout: true,
  baseURL: process.env.SERVER_BASE_URL,
  clientID: process.env.AUTH0_CLIENT_ID,
  issuerBaseURL: process.env.AUTH0_ISSUER_BASE_URL,
  secret: process.env.AUTH0_CLIENT_SECRET
};

const app = express()
const apiPort = 3000

app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())
app.use(auth(config))

db.on('error', console.error.bind(console, 'LOG::DEV::DATABASE::CONNECTION_ERROR'))

app.get('/', (req, res) => {
  res.send(req.oidc.isAuthenticated() ? 'Logged in' : 'Logged out')
})

app.use('/api', invitationRouter)

app.listen(apiPort, () => console.log(`LOG::DEV::SERVER::PORT::${apiPort}`))