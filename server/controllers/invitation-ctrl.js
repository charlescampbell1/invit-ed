const Invitation = require('../models/invitation-model')

createInvitation = (req, res) => {
  const body = req.body

  if (!body) { return res.status(400).json({ success: false, error: 'You must provide an invitation' }) }

  const invitation = new Invitation(body)

  if (!invitation) { return res.status(400).json({ success: false, error: err }) }

  invitation
    .save()
    .then(() => { return res.status(201).json({ success: true, id: invitation.id, message: 'Invitation created.' }) })
    .catch(error => { return res.status(400).json({ error, message: 'Invitation not created.' }) })
}

updateInvitation = async (req, res) => {
  const body = req.body

  if (!body) { return res.status(400).json({ success: false, error: 'You must provide a invitation to update.' }) }

  Invitation.findOne({ _id: req.params.id }, (err, invitation) => {
    if (err) { return res.status(404).json({ err, message: 'Invitation not found' }) }

    invitation.names = body.names
    invitation.location = body.location
    invitation.wedding_date = body.wedding_date
    invitation.notes = body.notes

    invitation
      .save()
      .then(() => { return res.status(200).json({ success: true, id: invitation._id, message: 'Invite updated.' }) })
      .catch(error => { return res.status(404).json({ error, message: 'Invitation not updated.' }) })
  })
}

deleteInvitation = async (req, res) => {
  await Invitation.findOneAndDelete({ _id: req.params.id }, (err, invitation) => {
    if (err) { return res.status(400).json({ success: false, error: err }) }
    if (!invitation) { return res.status(404).json({ success: false, error: `Invitation not found` }) }

    return res.status(200).json({ success: true, data: invitation })
  }).catch(err => console.log(err))
}

getInvitationById = async (req, res) => {
  await Invitation.findOne({ _id: req.params.id }, (err, invitation) => {
    if (err) { return res.status(400).json({ success: false, error: err }) }
    if (!invitation) { return res.status(404).json({ success: false, error: 'Invitation not found' }) }

    return res.status(200).json({ success: true, data: invitation })
  }).catch(err => console.log(err))
}

getInvitations = async (req, res) => {
  await Invitation.find({}, (err, invitations) => {
    if (err) { return res.status(400).json({ success: false, error: err }) }
    if (!invitations.length) { return res.status(404).json({ success: false, error: `Invitation not found` }) }
    return res.status(200).json({ success: true, data: invitations })
  }).catch(err => console.log(err))
}

module.exports = { createInvitation, updateInvitation, deleteInvitation, getInvitations, getInvitationById }